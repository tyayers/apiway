+++
title = "IoT Connected Car API Demo"
description = ""
tags = [
    "Project Kaleo"
]
date = "2019-08-15"
image = "car.jpg"
+++

This IoT Connected Car API demo showcases several Google Cloud services used togther to create a complete, end-to-end ecosystem use-case.  The IoT device in the form of a car can be controlled in the "IoT Car Simulator," where in a 3d web view you can drive around and the data is ingested into Google Cloud IoT Core, published through Pub/Sub to subscribers, including DataProc and BigQuery.  This data can then be queried and sold through APIs for ecosystem integrations, such as the Driver's Dashboard app.

{{<figure src="/images/digital-value-chain.png">}}

As you can see above, all parts of the *Digital Value Change* are represented, from the driver apps to the API Portal, APIs, and integration to the backend cloud microservices and databases.

The most fun about this project was just bringing all of the different cloud services together - from the backend database (BigQuery) to the IoT data ingestion (Cloud IoT Core) and API + hosting (Apigee & Firebase).  

## Architecture

{{<figure src="/images/kaleo-iot-architecture.png">}}

### BigQuery database

The BigQuery database is an ideal place to store all of the connected car iot telemetry data - it can scale infinately, and I can pull whatever data I need out of it to serve different apps through APIs.

{{<figure src="/images/kaleo-iot-bigquery.png">}}

## Apps

### Grand IoT Auto

The main app is the driving simulator ("*Grand IoT Auto*"), where you should definitely set your VIN Id when you open the app (real cars have this set in the factory :smiley:). Then you can drive around the 3d track, causing wear to your tires, and crashing into things.  Be careful though - the car is sending its telemetry data to Cloud IoT Core through the Connected Car API, and your *Driver Score* my be negatively impracted by careless and dangerous driving.

{{<figure src="/images/kaleo-iot-game.png">}}

Link: https://kaleoiot.firebaseapp.com/

### Driver Dashboard

The second app is the Driver Dashboard, where you can see your Driver Score and other data for your VIN.

{{<figure src="/images/kaleo-iot-dashboard.png">}}

Link: Click the *Driver Dashboard* button in the upper-right corner of the game.

### Leaderboard

The last app is the Leaderboard, where you can see which drivers have had the most accidents, all loaded from the BigQuery database which serves as the backend of this game.

{{<figure src="/images/kaleo-iot-leaderboard.png">}}

Link: Click the *Leaderboard* button in the upper-right corner of the game.

### Developer Portal

All of these apps and cloud services are tied together through the API layer, which is running in Apigee, and offering a developer API for the app and device integration.

Link: https://emea-poc13-kaleomedia.apigee.io/

{{<figure src="/images/kaleo-iot-portal.png">}}

## Source Code & More Information

If you like this demo, give it a star on my gitlab page:

https://gitlab.com/tyayers/kaleo-iot

## Credits
Photo by Uygar Kilic on Unsplash
