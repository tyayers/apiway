---
date: 2014-03-10
title: Kaleo Media
tags: [
  "Project Kaleo"
]
image: 'speakers.jpg'
---

Media companies usually have a ton of great content in their archives, but also have the same challenge that all companies in today's world have: how to engage developers, create and participate in ecosystems, and efficiently and effectively manage and enable access to their content without losing control.  Almost no media company can survive by just publishing its content on its own web site or in its own app - consumers expect to be able to consume content in their media app and platform of choice.

{{<figure src="/images/digital-value-chain.png">}}

Here we have all steps of the *Digital Value Chain*, from consumer apps on different devices, to a unified API layer, and integrations to various backend services (in this case themoviedb).

An API-First approach and API  Management can greatly help in this situation for the following reasons:

1. Content management systems are great at storing, indexing and streaming content, but not especially great at managing and securing access to that content, especially across all of today's channels and devices.

2. In the API world, enabling developers to integrate and innovate is key - that means opening up the media via APIs for consumption through both internal and external apps.  Using the same APIs for both use-cases insures that the papercuts and problems are fixed quickly - the more consumers there are, the more reason and resources to keep them happy, and grow the platform.

3. Accurately measuring and adjusting APIs based on real usage is key to building a user-base, and in the end delivering great user experiences.  Most content systems don't allow the flexible mixing and adapting of different data and media sources to provide the perfect consumer functionality, so API Management gives us the added flexiblity to monitor and adapt the APIs to deliver the best user experiences, even if we're not delivering the client app.

## Architecture

{{<figure src="/images/kaleo-media-architecture.png">}}

## Apps

### Hooli Media Streaming App

The Hooli Media Streaming App is a third party app that has bought access to the Kaleo Media APIs for their streaming content.

{{<figure src="/images/kaleo-media-hooli.png">}}

Link: https://tyayers.gitlab.io/kaleo-media/frontend/media-center-app/

### Media Chat App

The Media Chat App is another channel integration, this time only subscribing to Recommendations API to give movie recommendations.

{{<figure src="/images/kaleo-media-chat.png">}}

Link: https://tyayers.gitlab.io/kaleo-media/frontend/chat-app/

### Developer Portal

The Developer Portal links it all together, and allows any developer regardless if internal or external register and subscribe to the level of APIs needed for his/her project.

{{<figure src="/images/kaleo-media-portal.png">}}

Link: https://emea-poc13-kaleomedia.apigee.io/

## Source Code & More Information

https://gitlab.com/tyayers/kaleo-media


