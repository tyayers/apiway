+++
title = "Kaleo Retail"
description = ""
tags = [
    "Project Kaleo"
]
date = "2019-08-28"
image = "retail.jpg"
+++

Kaleo Retail shows how an e-commerce retailer can innovate in the logistics and delivery space by integrating real-time tracking of deliveries from logistics companies that can integrate via APIs into the supply chain.

It was really interesting to build a small E-Commerce site using VueJS for the frontend (and Materialize for the styling, which is an awesome Material Design frontend framework), and then having APIs in Apigee that integrated with the backend services (in this case a Firestore DB).  This shows the value of having the API layer available as the single point of contact for frontends (in this case the Kaleo Retail E-Commerce site and the logistics package tracking app).

## Architecture

{{< figure src="/images/kaleo-retail-architecture.png" >}}

## Apps & Source Code

https://github.com/igalonso/kaleo-retail/

### Credit

Photo by Alexandre Godreau on Unsplash
