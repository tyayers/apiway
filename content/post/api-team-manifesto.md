---
author: "Tyler Ayers"
date: 2019-12-03
title: The API Team Manifesto
image: 'artist.jpg'
weight: 3
tags: [
  "API Team"
]
aliases: [
  "/api-first"
]
---

## Introduction

{{<figure src="/images/api-first-revolution.png">}}

Often the biggest barrier to API and digital growth is in the mindset and organizational principles of a team. How is the team incentivized - are targets set according to internal, hidden KPIs, or according to live consumption data?  Does everyone from top to bottom understand the API economy and work towards designing, developing and releasing digital assets?  Are APIs even strategic company assets, or "only" technical agents of access?  Vision and team principles are important because they serve as reminders when life gets complicated, and tough choices have to be made regarding the allotment of precious time and resources.  

## We as an organization commit ourselves to these 7 API Team Principles:

### 1. Scale 
APIs are the central nervous system of our organization, both internally and to our customers and partners.  Every cent we earn flows at some point through our APIs.  That's why we treat APIs as the most important products that we design, develop, release and improve.  Our APIs have lifecycles just like any other product on the market.

### 2. Self-Service
Every service in our organization, either internal or external, is consumed through documented, versioned, and managed APIs with common identity, security and operations policies, and with automatic self-service onboarding.  No exceptions. 

### 3. Transparency
Our API Products are contracts with our customers and partners, and are documented and curated in professional API Product Portals, where anyone can check any detail or change, anytime.

### 4. Innovation
API Products are the stable center of our business, and enable decoupled, flexible and dynamic innovation for both clients and backends, both internally and externally. 

### 5. Improvement
We leverage API telemetric data to measure our success, and base our future business planning on how our API Products are actually used in the real world.  Consumption is the measurement that we use the most - which APIs are consumed most, and how each API develops in terms of consumption from hour to hour, day to day, and month to month.

### 6. Change
All of our API Products have lifecycles, and API Products that no longer deliver business value are put on a path to deprecation with transparency, honesty, and a realistic migration path to better performing APIs.

### 7. Value
Ecosystems are more important than any single product or API, and API Products help us create ecosystems of value that benefit our customers, our partners, and ourselves (in that order).

Join us!

______________________________
(Signatures)

Your API team