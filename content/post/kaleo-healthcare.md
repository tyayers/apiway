---
date: 2020-04-21
title: "Kaleo Healthcare"
image: 'healthcare.jpg'
tags: [
  "Project Kaleo"
]
---

# Exploring Low-Code Apps & APIs in Healthcare

AppSheet is one of the latest additions to the Google Cloud family, and it's been really interesting trying out various low-code / no-code solutions with AppSheet apps and Apigee APIs. This example is based on a [Covid Hackathon solution](https://www.appsheet.com/samples/Resource-selfreporting-for-county-or-statewide-systems?appGuidString=78a5e4d4-2740-44bb-b5ab-476c4e2dc2d4) that was created to show how AppSheet could be used to effectively manage hospital supplies across locations.

{{<figure src="/images/kaleo-health-appsheet.gif">}}

The solution uses Google Sheets as a backend, and in the typical AppSheet way offers a really user-friendly way to maintain, update and view the supply data.

I started investigating how we could tap into that data from an API proxy as well, so in this case integrating with the same Google Sheet on the backend, and then offering REST APIs for other clients to integrate and view the data. The JSON data from the sheet was very complex, with all rows and cells listed in a flat array structure - basically impossible to integrate well in a frontend application (without AppSheet to bridge the gap).

{{<figure src="/images/kaleo-health-data.gif">}}

You can view the data for yourself [here](https://spreadsheets.google.com/feeds/cells/1EBWda4IXKSBBDPRC_fth-xS-eowi1qS34Jy5ueXYwhg/1/public/full?alt=json).

So how can we offer the data through a simple and structured API without writing code or spinning up infrastructure resources somewhere?

## The Low-Code API Solution

An API Management solution can give us the platform to integrate with this data, and offer clean, fast APIs to consumers. The first step is to define an API spec that brings order to the table/cell chaos.

{{<figure src="/images/kaleo-health-spec.png">}}

API spec design is both an art and a science  -  a well-designed API provides a clean interface for clients to call, instead of querying the backend database directly (or the Google Sheet).

The next step is to spin up our proxy, which is basically a managed endpoint for clients to connect to, and a place where we can inject policies, logic, and integration configuration with the backend data.

{{<figure src="/images/kaleo-health-proxy-design.png">}}

Here we can configure policies to shape the traffic, and integrate with backend data.

The API proxy is where the magic happens, and we can apply rate limiting, key checks, and integrate / cache data from our backend. This configuration console in Apigee is a good example of how this can be done with little effort.

After this, we have a deployed endpoint, and integration configuration with our backend data. Here we can see the deployed endpoint, as well as the target connection to our Google Sheet for data.

{{<figure src="/images/kaleo-health-proxy.png">}}

API proxy deployment information, including endpoint and status, and target Google Sheet connection.

## Architecture (or "Can It Be This Simple?")

{{<figure src="/images/kaleo-health-arch.png">}}

Easy architecture for both app-based (AppSheet) & API-based data access.

## Accelerating the Clients

After deploying the proxy, here's a first web client that can connect and query for hospital supply data.

{{<figure src="/images/kaleo-health-app.gif">}}

As you can see, the data access is relatively slow, with requests taking 500–700ms. This is because the Google Sheet data is being queried, and it is quite a lot of data to sift through. So to optimize this, we can easily add a Response Cache policy to the proxy, which automatically caches the data query results, and lets us avoid checking the backend if we already have accessed the data within the last hour (default, can be changed).

Here's the app with caching enabled in the proxy:

{{<figure src="/images/kaleo-health-app-cached.gif">}}

So as you can see here, it only takes 70–150 ms with cached data, which is significantly better, with basically no effort.

## Conclusion

Do you ever see situations where it would be useful to track assets and resources digitally, where today only analog and inefficent methods are available? I see stuff like this all the time, and hope that this example of easily making data available in apps (like with AppSheet) and API clients (with API proxies) with little to no code or infrastructure is a useful example.

Because remember, friends don't let friends inefficiently access data.

You can see the source code [here](https://gitlab.com/tyayers/kaleo-healthcare), test the AppSheet app template [here](https://www.appsheet.com/samples/Resource-selfreporting-for-county-or-statewide-systems?appGuidString=78a5e4d4-2740-44bb-b5ab-476c4e2dc2d4), and play with my web client [here](https://tyayers.gitlab.io/kaleo-healthcare).

## Credits
Photo: https://unsplash.com/photos/Xruf17OrkwM
