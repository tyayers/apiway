+++
title = "About Project Kaleo"
date = "2020-01-09"
image = 'boats.jpg'
weight = 2
tags = [
  "Project Kaleo"
]
+++

Project Kaleo is a project for me to work on creative cloud & API use cases, and try out some cool technology and services to realize those use-cases.  The use-cases usually are inspired by some experience or projects that I've encountered in some industry, for example airports or retail.  If the resulting projects are useful for demos or something else, then that's even better.

What does the name "Kaleo" mean?  It means "to call or summon" in Greek, and that's what we need sometimes to get inspiration for what's possible with the awesome digital tools at our fingertips these days.

Link to all Project Kaleo projects: https://theapiway.com/tags/project-kaleo/

## Credits
Photo by Ivan F on Unsplash
