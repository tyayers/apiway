---
date: 2019-11-24
title: Mini-Hipster Webshop
tags: [
    "Project Kaleo"
]
image: 'hipster-bike.jpg'
---

The Mini-Hipster Webshop demo was forked from the Hipster Webshop project, which started out as a microservices demo project for Kubernetes and Istio.  The original project demonstrated the full power of microservices by including services in Go, C#, Python, NodeJS and Ruby.  While this diversity was really impressive and realistic, it made using and extending it difficult, so this is a rewritten version just in Node.js, and with HTTPS communication between the microservices.

{{<figure src="/images/digital-value-chain.png">}}

The *Digital Value Chain* is covered end-to-end through the Frontend Service providing the HTML web app, all the way through the APIs and backend services covering the product, currency and recommendation services.  Istio helps in this architecture by providing the resource configuration and modules for network routing, and access policies through the Apigee Istio modules.

## Architecture

{{<figure src="/images/kaleo-hipster-architecture.png">}}

## Apps

### Mini-Hipster Webshop

The internal web portal team povides all of the in-house portal features, and integrates with the same APIs as external and ecosystem developers.  As an additional feature, the Istio network routing policy contains A/B routing to 2 backend versions of the Frontend service (50/50 routing).  The new Frontend service is advertising a new area of the webshop called "Hipster Kids" with hipster products for the little ones.

{{<figure src="/images/kaleo-hipster-frontend.png">}}

## Source Code & More Info

https://github.com/tyayers/hipster-apim-demo

## Credit
Photo by Markus Spiske on Unsplash
