---
date: 2019-11-10
title: Smart-Credit
tags: [
    "Project Kaleo"
]
image: 'credit.jpg'
---

The Smart-Credit demo shows a complete ecosystem example of how a fictional financing company, Smart-Credit, opens up their credit APIs for both their internal portal developer team, as well as ecosystem teams in other apps - for example a chat app.  Smart-Credit also allows ecosystem insurance companies to register their services with Smart-Credit in a completely automated way, and integrate insurance offerings into the Smart-Credit APIs.

{{<figure src="/images/digital-value-chain.png">}}

The *Digital Value Chain* is covered from the end-user apps in the form of the credit application web-site and chat app, to the API layer and finally integrating with the backend services providing the image processing and credit calculation services.

Smart-Credit goes above and beyond the standard credit calculation features by offering image recognition and processing features based on machine learning algorithms and data, which is a great added value to customers.

## Architecture

{{<figure src="/images/smart-credit-architecture.png">}}

## Apps

### Smart-Credit Web Portal

The internal web portal team povides all of the in-house portal features, and integrates with the same APIs as external and ecosystem developers.  

{{<figure src="/images/kaleo-credit-web.png">}}

Link: https://smart-credit-cfc36.firebaseapp.com/

### Credit Chat App

The Credit Chat App is an example of a 3rd party ecosystem app that uses the same Smart-Credit APIs to offer functionality in channel apps, which can reach even more users.

{{<figure src="/images/kaleo-credit-chat.png">}}

Link: https://emea-poc13-smartcredit.apigee.io/

### Smart-Credit Developer Portal

The Developer Portal contains all APIs, documentation and registration information for both internal as well as external developers, who can then get up and running fast using the 5-5-5 rule (5 minutes to understand an API, 5 hours to code a first app, and 5 days to release it to the world).

{{<figure src="/images/kaleo-credit-portal.png">}}

Link: https://emea-poc13-smartcredit.apigee.io/

## Source Code & More Info

https://github.com/tyayers/smart-credit

## Credit
Photo by Tierra Mallorca on Unsplash
