---
date: 2014-03-10
title: "Digital Airport Security"
image: 'airport.jpg'
tags: [
  "Project Kaleo"
]
---

The Digital Airport Security demo shows how Google Cloud Vision AI as well as Google AutoML services can be used to recognize crowds, and trigger appropriate alerts accordingly, in this case to alert passengers to increased congestion.

{{<figure src="/images/digital-value-chain.png">}}

The *Digital Value Chain* is covered end-to-end through the consumer app view (boarding app & alerts) to the APIs hosted in Apigee, and then integrated with various backend services, including the image recognition in the GOogle Vision and AutoML services.

The fun part of this demo was using the Google Vision and AutoML services - awesome stuff!  Especially the AutoML service, which enables you to build custom vision recognition services using your own image datasets is just awesome.

## Architecture

{{<figure src="/images/kaleo-airport-architecture.png">}}

### Google Cloud AutoML Vision

The AutoML Vision service is particularly useful because we can train the dataset to recoginize the different locations and conditions, and so get much more specific than the normal Google Cloud Vision API.

{{<figure src="/images/kaleo-airport-automl.png">}}

## Apps

### Airport Security Dashboard

The Airport Security Dashboard app shows live footage, or pre-loaded test footage, of security cameras of the queuing areas, and analyzes the footage in real-time for crowd congestion signals.  The demo uses two algorithms for the image analysis: 1) Google Cloud Vision API for generic recognition of objects, and 2) Google AutoML for custom trained model recognition of specific patterns (such as groups of people).

{{<figure src="/images/kaleo-airport-security-app.png">}}

Link: https://airport-security.web.app/

### Airline App

The example airline app shows how alerts broadcast from the airport can be used to give passengers useful real-time updates about things that matter to them, for example how long the airport security queue will be when they get there.  This live analysis and information could easily be extended with ML results from historical queue data for even more useful alerts.

{{<figure src="/images/kaleo-airport-airline-app.png">}}

Link: https://airport-security.web.app/airline-app.html

### Developer Portal

The developer portal is the key to simple and easy ecosystem integrations - when clean, effective API specifications are published for all to see and use, we are revolutionizing the usually awful topic of service-to-service integration. And when developer integrations are easy and effective, then word spreads, and by making developers happy we are literally accelerating usage and growth.

{{<figure src="/images/kaleo-airport-portal.png">}}

Link: https://tyayers-eval-airportdeveloperportal.apigee.io/

## Source Code & More Information

If you like this demo, give it a star on my GitLab page:

https://github.com/tyayers/digital-airport

## Credits
Photo by Ken Yam on Unsplash
