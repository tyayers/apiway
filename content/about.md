+++
title = "About"
date = "2019-01-01"
image = 'read.jpg'
weight = 1
tags = [
    "API Way"
]
+++

APIs and cloud technologies have fundamentally changed and improved how we as users, developers, business stakeholders, and organizations interact with technology.  Increasingly everything is just integrated and works together, and what a magical feeling that is!  And that should be our mission and goal: make integrations and seamless digital interactions commonplace, and for that we rely on APIs and cloud technologies.

The API Way is a blog about designing, coordinating and implementing APIs as a core mission of a team and organization.  I am convinced that every organization on the planet should have APIs firmly set as one of their highest priorities, and API KPIs should form some of the key indicators that a team has for being successful in the 21st century. 

The posts here are roughly structured in these categories:

* **Project Kaleo** - Kaleo detail projects demonstrating a use-case applying APIs and other cloud technologies to solve some real-world business problem.
* **API Team** - Maintaining priority and focus on APIs is no small feat for a team, and posts here should help orient and give direction to API teams.

In case you find something useful, or have an idea for a post or project, let me know [@theapiway](https://twitter.com/theapiway).

Full disclosure: I work in the cloud and API space, but all views expressed here are purely my own.
